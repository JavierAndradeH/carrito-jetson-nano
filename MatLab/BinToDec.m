function Res = RedBinToDec (vector)
   Res = vector (1) * 2^3 + vector(2) * 2^2 + vector(3) * 2 + vector(4);
end