import jetson.inference
import jetson.utils
import time 
import cv2
import numpy as np

timeStamp = time.time()
fpsFilt = 0

# Se establece la conexión con nuestro modelo entrenado convertido a ONNX
net = jetson.inference.detectNet(model="models/Modelo_FondoBlanco/ssd-mobilenet.onnx",  # Ubicación del modelo entranado
                                labels="models/Modelo_FondoBlanco/labels.txt", # Ubicación del archivo que contiene las clases.
                                input_blob="input_0", output_cvg="scores", output_bbox="boxes", 
                                threshold = .6) # Umbral que se utiliza para decidir si consideramos que un objeto ha sido detectado o no.
dispW = 1280
dispH = 720
flip = 0 # Se establece en 0, cambiar a 1 para voltear la camara.
font = cv2.FONT_HERSHEY_SIMPLEX 

# Se configura la camara
camSet='nvarguscamerasrc wbmode=3 tnr-mode=2 tnr-strength=1 ee-mode=2 ee-strength=1 !  video/x-raw(memory:NVMM), width=3264, height=2464, format=NV12, framerate=21/1 ! nvvidconv flip-method='+str(flip)+' ! video/x-raw, width='+str(dispW)+', height='+str(dispH)+', format=BGRx ! videobalance contrast=1.5 brightness=-.2 saturation=1.2 ! videoconvert ! video/x-raw, format=BGR ! appsink'
cam=cv2.VideoCapture(camSet)

while True:
    _,imagenCamara = cam.read() 
    alturaCamara = imagenCamara.shape[0]
    anchoCamara = imagenCamara.shape[1]

    # Se hace una conversión de la camara de BGR a RGBA
    frame = cv2.cvtColor(imagenCamara, cv2.COLOR_BGR2RGBA).astype(np.float32) # Tomamos el cuadro (frame)

    # Hacemos conversión a un objeto cudaImage para que pueda ser leido
    frame = jetson.utils.cudaFromNumpy(frame)

    detecciones = net.Detect(frame, anchoCamara, alturaCamara) #Hacemos la detección
    for deteccion in detecciones:
        # print (deteccion) Visualizar el objeto completo
        ID = deteccion.ClassID # Identificador de la clase
        top = int(deteccion.Top)
        left = int(deteccion.Left)
        bottom = int(deteccion.Bottom)
        right = int(deteccion.Right)
        item = net.GetClassDesc(ID) # Aquí obtiene el nombre del la clase identificada
        # print (item, top, left, bottom, right) Visualizar coordenadas
        # NOTE Aquí al conocer la ubicacion del objeto a detectar que se haya entrenado, se puede desplazar al carrito hacía él.

        # Dibujar el rectangulo; Tupla de esquina superior izquieda y esquina inferior derecha
        cv2.rectangle(imagenCamara,(left, top), (right, bottom), (0,255,0), 1) # Color del rectangulo y grosor del borde

        # Se coloca el texto arriba de la esquina superior izquierda
        cv2.putText(imagenCamara, item, (left, top + 20), font, .75,(0,0,255), 2) # Fuente y color del texto

    dt = time.time() - timeStamp
    timeStamp = time.time()
    fps = 1/dt
    fpsFilt = .9 * fpsFilt + .1 * fps
    cv2.putText(imagenCamara, str(round(fpsFilt, 1)) + ' fps', (0, 30), font, 1, (0,0,255), 2) #Se colocan los fps (fotogramas por sergundo) en pantalla
    cv2.imshow('Detección Números', imagenCamara)
    cv2.moveWindow('Detección Números', 0,0)
    if cv2.waitKey(1) == ord('q'):
        break
cam.release()
cv2.destroyAllWindows()